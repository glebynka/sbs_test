﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[InitializeOnLoad()]
public class WaypointEditor
{
    [DrawGizmo(GizmoType.NonSelected | GizmoType.Selected | GizmoType.Pickable)]
    public static void OnDrawSceneGizmo(Waypoint waypoint, GizmoType gizmoType)
    {
        Gizmos.color = Color.black;

        var parentOfWaypoint = waypoint.transform.parent;

        if (parentOfWaypoint == null) 
            return;
        
        var children = parentOfWaypoint.GetComponentsInChildren<Transform>();

        for (int i = 1; i < children.Length; i++)
        {
            var nextIndex = i + 1;
            
            if (nextIndex < children.Length)
            {
                Gizmos.DrawLine(children[i].position, children[nextIndex].position);
            }
        }
    }
}
#endif