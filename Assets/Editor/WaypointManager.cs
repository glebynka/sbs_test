﻿#if UNITY_EDITOR
using System.Reflection;
using UnityEditor;
using UnityEngine;

public class WaypointManager : EditorWindow
{
    [MenuItem("Tools/Waypoint Editor")]
    public static void Open()
    {
        GetWindow<WaypointManager>();
    }

    [SerializeField] private Transform _waypointRoot;

    private void OnGUI()
    {
        SerializedObject obj = new SerializedObject(this);

        EditorGUILayout.PropertyField(obj.FindProperty("_waypointRoot"));

        if (_waypointRoot == null)
        {
            EditorGUILayout.HelpBox("Root transform must be selected !", MessageType.Warning);
        }
        else
        {
            EditorGUILayout.BeginVertical("box");
            DrawButtons();
            EditorGUILayout.EndVertical();
        }

        obj.ApplyModifiedProperties();
    }

    private void DrawButtons()
    {
        if (GUILayout.Button("Create Waypoint"))
        {
            CreateWaypoint();
        }

        if (GUILayout.Button("Clear Waypoints"))
        {
            ClearWaypoints();
        }
    }

    private void ClearWaypoints()
    {
        var waypointChildren = _waypointRoot.GetComponentsInChildren<Waypoint>();

        foreach (var child in waypointChildren)
        {
            DestroyImmediate(child.gameObject);
        }
    }

    private void CreateWaypoint()
    {
        GameObject waypointObject = new GameObject($"Waypoint {_waypointRoot.childCount}");

        var waypoint = waypointObject.GetComponent<Waypoint>();
        if (!waypoint)
        {
            waypoint = waypointObject.gameObject.AddComponent<Waypoint>();
        }

        waypoint.transform.parent = _waypointRoot;
        waypoint.SetIndex(_waypointRoot.childCount - 1);
        waypoint.name = $"Waypoint_{_waypointRoot.childCount - 1}";
        waypoint.transform.position = GetPreviousPosition(waypoint);

        SetIcon(waypoint.gameObject);

        Selection.activeGameObject = waypoint.gameObject;
    }

    private void SetIcon(GameObject currentObject)
    {
        Texture2D icon = (Texture2D) Resources.Load("Icons/circle-outline-of-small-size");

        var editorGUIUtilityType = typeof(EditorGUIUtility);
        var bindingFlags = BindingFlags.InvokeMethod | BindingFlags.Static | BindingFlags.NonPublic;
        var args = new object[] {currentObject, icon};
        editorGUIUtilityType.InvokeMember("SetIconForObject", bindingFlags, null, null, args);
    }

    private Vector3 GetPreviousPosition(Waypoint waypoint)
    {
        var allWaypoins = _waypointRoot.GetComponentsInChildren<Waypoint>();

        foreach (var currentWaypoint in allWaypoins)
        {
            if (currentWaypoint.WaypointIndex == waypoint.WaypointIndex - 1)
            {
                return currentWaypoint.transform.position;
            }
        }

        return Vector3.zero;
    }
}
#endif