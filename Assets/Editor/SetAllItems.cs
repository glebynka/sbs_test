﻿#if  UNITY_EDITOR

using Data;
using UnityEditor;
using UnityEngine;

namespace Editor
{
   [CustomEditor(typeof(ItemsImageData))]
   public class SetAllItems : UnityEditor.Editor
   {
      public override void OnInspectorGUI()
      {
         DrawDefaultInspector();

         ItemsImageData imageData = (ItemsImageData) target;

         if (GUILayout.Button("Update All Items"))
         {
            imageData.UpdateAllItems();
         }
      }
   }
}

#endif


