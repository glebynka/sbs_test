﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    public int WaypointIndex => _waypointIndex;
    
    [SerializeField] private int _waypointIndex;

    public void SetIndex(int indexValue)
    {
        _waypointIndex = indexValue;
    }
}
