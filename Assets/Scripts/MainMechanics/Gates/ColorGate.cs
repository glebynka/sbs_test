﻿using UnityEngine;

public class ColorGate : Gate
{
    [SerializeField] private Colors _colors;

    protected override void ChangeGateItem(int itemIndex = 0)
    {
        _colors = _colors.Next();
        itemIndex = (int) _colors;
        Debug.Log($"Current type: {_colors.ToString()}");
        
        base.ChangeGateItem(itemIndex);
    }

    protected override int GetGateIndex()
    {
        return (int) _colors;
    }

    protected override bool CheckItemType(Item itemType)
    {
        return (itemType as ColorItem);
    }
}
