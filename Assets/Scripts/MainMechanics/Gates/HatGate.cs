﻿using UnityEngine;

public class HatGate : Gate
{
    [SerializeField] private Hats _hatsType;
    
    protected override void ChangeGateItem(int itemIndex = 0)
    {
        _hatsType = _hatsType.Next();
        itemIndex = (int) _hatsType;
        //Debug.Log($"Current type: {_hatsType.ToString()}");
        
        base.ChangeGateItem(itemIndex);
    }

    protected override int GetGateIndex()
    {
        return (int) _hatsType;
    }
    
    protected override bool CheckItemType(Item itemType)
    {
        return (itemType as HatItem);
    }
}
