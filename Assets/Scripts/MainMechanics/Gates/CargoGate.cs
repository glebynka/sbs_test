﻿using System;
using System.Collections;
using System.Collections.Generic;
using Data;
using UnityEngine;

public class CargoGate : Gate
{
    [SerializeField] private Cargos _cargosType;
    
    protected override void ChangeGateItem(int itemIndex = 0)
    {
        _cargosType = _cargosType.Next();
        itemIndex = (int) _cargosType;
        Debug.Log($"Current type: {_cargosType.ToString()}");
        
        base.ChangeGateItem(itemIndex);
    }

    protected override int GetGateIndex()
    {
        return (int) _cargosType;
    }
    
    protected override bool CheckItemType(Item itemType)
    {
        return (itemType as CargoItem);
    }
}
