﻿using System;
using Data;
using UnityEngine;

public abstract class Gate : MonoBehaviour
{
    public bool IsGateOpen => _isGateOpen;

    [SerializeField] protected ItemsImageData _imageData;
    
    private SpriteRenderer _gateRenderer;
    private Item _targetItem;
    private bool _isGateOpen;
    
    private void Awake()
    {
        _gateRenderer = GetComponent<SpriteRenderer>();
        _gateRenderer.sprite = _imageData.ItemsImage[0];
    }

    protected virtual void ChangeGateItem(int itemIndex = 0)
    {
        _gateRenderer.sprite = _imageData.ItemsImage[itemIndex];
    }

    protected abstract int GetGateIndex();

    protected abstract bool CheckItemType(Item itemType);

    private bool CheckItemValue(Item currentItem)
    {
        _isGateOpen = GetGateIndex() == currentItem.GetItemIndex();
        return _isGateOpen;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var item = other.GetComponent<Item>();

        if (!CheckItemType(item))
            return;

        _targetItem = item;

        EventsObserver.Publish(new GateEvent(CheckItemValue(item)));
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        _targetItem = null;
    }

    private void OnMouseDown()
    {
        ChangeGateItem();

        if (_targetItem == null)
            return;
        
        EventsObserver.Publish(new GateEvent(CheckItemValue(_targetItem)));
    }
}