﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "DataItem", menuName = "SO/DataItem", order = 1)]
    public class ItemsImageData : ScriptableObject
    {
        public Sprite[] ItemsImage => _itemsImage;

        [SerializeField] private Sprite[] _itemsImage;

        public void UpdateAllItems()
        {
            string nameOfEnum = $"{name}";
            string path = Application.dataPath + $"/Scripts/MainMechanics/Enums/{nameOfEnum}.cs";
            string content = $"public enum {nameOfEnum}" + " \n {";

            for (int i = 0; i < _itemsImage.Length; i++)
            {
                content += $"\n\t{_itemsImage[i].name}" + $" = {i},";
            }

            File.WriteAllText(path, " " + content.Remove(content.Length - 1) + " \n }");
            AssetDatabase.Refresh();

            Debug.Log($"Items {name} was updated");
        }
    }
}