﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelDataManager", menuName = "SO/LevelDataManager", order = 2)]
public class LevelDataManager : ScriptableObject
{
    [SerializeField] private int _currentLevelIndex;
    [SerializeField] private LevelData[] _levels;

    public LevelData CurrentLevel => _levels[_currentLevelIndex];
}