﻿using System;
using UnityEngine;

[Serializable]
public struct LevelConfig
{
    public Transform Route => _route;
    public Transform Gates => _gates;
    public int SheepCount => _sheepCount;
    public GameObject SheepPrefab => _sheepPrefab;
    public Sprite LevelBackround => _levelBackround;
    
    [Header("[SHEEP PARAMETERS]")]
    [SerializeField] private GameObject _sheepPrefab;
    [SerializeField] private int _sheepCount;

    [Header("[LEVEL PARAMETERS]")] 
    [SerializeField] private Sprite _levelBackround;
    [SerializeField] private Transform _route;
    [SerializeField] private Transform _gates;
}

[CreateAssetMenu(fileName = "LevelData", menuName = "SO/LevelData", order = 3)]
public class LevelData : ScriptableObject
{
    public LevelConfig LevelConfig => _levelConfig;
    
    [SerializeField] private LevelConfig _levelConfig;
    
    public Transform[] GetWaypointsOfRoot()
    {
        var waypoints = _levelConfig.Route.GetComponentsInChildren<Waypoint>();

        if (waypoints.Length > 0)
        {
            var waypointTransforms = new Transform[waypoints.Length];

            for (int i = 0; i < waypoints.Length; i++)
            {
                waypointTransforms[i] = waypoints[i].transform;
            }

            return waypointTransforms;
        }

        return null;
    }

    public float[] GetRootDistances()
    {
        var points = GetWaypointsOfRoot();
        var distBetweenPoints = new float[points.Length - 1];

        for (var i = 0; i < distBetweenPoints.Length; i++)
        {
            distBetweenPoints[i] = Vector2.Distance(points[i].position, points[i + 1].position);
        }

        return distBetweenPoints;
    }

    public Gate[] GetGates()
    {
        var gates = _levelConfig.Route.GetComponentsInChildren<Gate>();
        
        return gates.Length > 0 ? gates : null;
    }
}