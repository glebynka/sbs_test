﻿using System.Collections.Generic;
using UnityEngine;

public abstract class BaseSpawner : MonoBehaviour
{
    [SerializeField] protected LevelDataManager _levelDataManager;

    private void Start()
    {
        Create();
    }

    protected abstract void Create();

    private GameObject SpawnOnly(GameObject spawnObject)
    {
        var currentObject = Instantiate(spawnObject, transform, true);
        return currentObject;
    }

    protected virtual GameObject SimpleSpawn(GameObject spawnObject)
    {
        var currentObject = SpawnOnly(spawnObject);
        currentObject.transform.SetPositionAndRotation(Vector2.zero, Quaternion.identity);

        return currentObject;
    }

    protected virtual GameObject SimpleSpawn(GameObject spawnObject, Vector2 customPosition)
    {
        var currentObject = SpawnOnly(spawnObject);
        currentObject.transform.SetPositionAndRotation(customPosition, Quaternion.identity);

        return currentObject;
    }

    protected virtual GameObject SimpleSpawn(GameObject spawnObject, Vector2 customPosition, Transform parent)
    {
        var currentObject = SimpleSpawn(spawnObject, customPosition);
        currentObject.transform.SetParent(parent);

        return currentObject;
    }
}