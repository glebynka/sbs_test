﻿using UnityEngine;

public class LevelSpawner : BaseSpawner
{
    protected override void Create()
    {
        SpawnRoute();
        SpawnGates();
        SpawnBackGround();
    }

    private void SpawnGates()
    {
        SimpleSpawn(_levelDataManager.CurrentLevel.LevelConfig.Gates.gameObject);
    }

    private void SpawnRoute()
    {
        SimpleSpawn(_levelDataManager.CurrentLevel.LevelConfig.Route.gameObject);
    }

    private void SpawnBackGround()
    {
        var backGround = new GameObject("BackGround");
        var spriteRendererBackground =  backGround.AddComponent<SpriteRenderer>();
        
        spriteRendererBackground.transform.SetParent(transform);
        spriteRendererBackground.transform.SetPositionAndRotation(Vector3.zero , Quaternion.identity);
        spriteRendererBackground.sprite = _levelDataManager.CurrentLevel.LevelConfig.LevelBackround;
    }
}