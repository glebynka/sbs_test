﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SheepSpawner : BaseSpawner
{
    [SerializeField] private float _delaySpawn = 1.0f;

    public List<SheepBehaviour> Sheeps => _sheeps;
    protected List<SheepBehaviour> _sheeps = new List<SheepBehaviour>();

    private IEnumerator ActivatingSheep()
    {
        for (var i = _sheeps.Count - 1; i >= 0; i--)
        {
            _sheeps[i].gameObject.SetActive(true);
            yield return new WaitForSeconds(_delaySpawn);
        }
    }

    private void OnEnable()
    {
        EventsObserver.AddEventListener<SheepFinishedEvent>(DeleteSheep);
    }

    private void OnDisable()
    {
        EventsObserver.RemoveEventListener<SheepFinishedEvent>(DeleteSheep);
    }

    protected override void Create()
    {
        SheepSpawn();
        ActivateSheep();
    }

    private void SheepSpawn()
    {
        var spawnPoint = _levelDataManager.CurrentLevel.GetWaypointsOfRoot()[0]; // firstPoint

        for (var i = 0; i < _levelDataManager.CurrentLevel.LevelConfig.SheepCount; i++)
        {
            var sheepPrefab = SimpleSpawn(_levelDataManager.CurrentLevel.LevelConfig.SheepPrefab, spawnPoint.position, transform);
            var sheep = sheepPrefab.GetComponent<SheepBehaviour>();

            sheep.name += i.ToString();
            sheep.enabled = false;

            if (!sheep) return;
            
            sheepPrefab.SetActive(false);
            
            _sheeps.Add(sheep);
        }
    }

    private void ActivateSheep()
    {
        StartCoroutine(ActivatingSheep());
    }
    
    private void DeleteSheep(SheepFinishedEvent e)
    {
        _sheeps.Remove(e.Sheep);
        Destroy(e.Sheep.gameObject);
    }
}