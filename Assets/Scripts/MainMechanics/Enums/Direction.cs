﻿public enum Direction
{
    front = 0,
    back = 1,
    left = 2,
    right = 3
}