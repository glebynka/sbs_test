﻿public enum SheepState
{
    Stop = 0,
    Sleep = 1,
    Walking = 2
}