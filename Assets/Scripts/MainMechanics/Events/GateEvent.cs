﻿using UnityEngine;

// This class demonstrate how can use Observer
public class GateEvent : IEvent
{
    public bool IsOpened { get; }

    public GateEvent(bool isOpened) //constructor for this event
    {
        IsOpened = isOpened;
        Debug.Log(IsOpened ? "Gate open" : "Gate closed");
    }
}
