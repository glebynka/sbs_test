﻿
using UnityEngine;

public class SheepFinishedEvent : IEvent
{
   public SheepBehaviour Sheep { get; }

   public SheepFinishedEvent(SheepBehaviour sheep)
   {
      Sheep = sheep;
      Debug.Log($"I am {sheep.name} finished level");
   }
}
