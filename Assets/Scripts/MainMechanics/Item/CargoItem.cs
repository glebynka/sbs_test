﻿using UnityEngine;

public class CargoItem : Item
{
    [SerializeField] private Cargos _cargosType;
    
    public override int GetItemIndex()
    {
        return (int) _cargosType;
    }

    protected override void InitSprite(int spriteIndex = 0)
    {
        base.InitSprite((int)_cargosType);
    }
}
