﻿using UnityEngine;

public class ColorItem : Item
{
    [SerializeField] private Colors _colorType;
    
    protected override void InitSprite(int spriteIndex = 0)
    {
        base.InitSprite((int)_colorType);
    }
    public override int GetItemIndex()
    {
        return (int) _colorType;
    }
}
