﻿using System;
using Data;
using UnityEngine;

public abstract class Item : MonoBehaviour
{
  [SerializeField] protected ItemsImageData ImageData;
  private SpriteRenderer _itemRenderer;

  private void Awake()
   {
       InitSprite();
   }
   public abstract int GetItemIndex();

   protected virtual void InitSprite(int spriteIndex = 0)
   {
       _itemRenderer = GetComponent<SpriteRenderer>();
       _itemRenderer.sprite =  ImageData.ItemsImage[spriteIndex];
   }

}
