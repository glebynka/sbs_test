﻿using UnityEngine;

public class HatItem : Item
{
    [SerializeField] private Hats _hatsType;

    protected override void InitSprite(int spriteIndex = 0)
    {
        base.InitSprite((int)_hatsType);
    }

    public override int GetItemIndex()
    {
        return (int) _hatsType;
    }
    
}
