﻿using System;
using UnityEngine;

public class MovementState : BaseState
{
    private SheepBehaviour _sheepBehaviour;
    private float _stopDistance = 0.2f;
    
    // private Transform[] _waypoints;
    // private Gate[] _gates;
    // private float[] _distanceBetweenWaypoints;
    
    public override Type Tick()
    {
     //for (var i = 0; i < _sheepSpawner.Sheeps.Count; i++)
     //{
     //    var nextIndex = i + 1;

     //    var distBetweenSheeps = (nextIndex < _sheepSpawner.Sheeps.Count)
     //        ? Vector2.Distance(_sheepSpawner.Sheeps[i].transform.position,
     //            _sheepSpawner.Sheeps[nextIndex].transform.position)
     //        : _stopDistance;

     //    if (distBetweenSheeps >= _stopDistance)
     //    {
     //        _sheepSpawner.Sheeps[i].MoveSheep(_waypoints, _distanceBetweenWaypoints);
     //    }

     //    if (_gates.Length > 0)
     //    {
     //        for (var j = 0; j < _gates.Length; j++)
     //        {
     //            if (!_gates[i].IsGateOpen)
     //            {
     //                return typeof(StopState);
     //            }
     //        }
     //    }
     //}
     //  
        return null;
    }

    public MovementState(SheepBehaviour sheep) : base(sheep.gameObject)
    {
        _sheepBehaviour = sheep;
    }
}