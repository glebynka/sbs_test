﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopState : BaseState
{
    public override Type Tick()
    {
        Debug.Log($"Stop state");
        return null;
    }

    public StopState(GameObject gameObject) : base(gameObject)
    {
    }
}