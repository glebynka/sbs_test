﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public Dictionary<Type, BaseState> AvailableState => _availableState;
    private Dictionary<Type, BaseState> _availableState = new Dictionary<Type, BaseState>();

    public BaseState CurrentState { get; private set; }
    public event Action<BaseState> OnStateChanged;

    private void Update()
    {
        if (CurrentState == null)
        {
            CurrentState = _availableState.Values.First();
        }

        var nextState = CurrentState?.Tick();

        if (nextState != null && nextState != CurrentState?.GetType())
        {
            SwitchToNewState(nextState);
        }
    }

    private void SwitchToNewState(Type nextState)
    {
        CurrentState = _availableState[nextState];
        OnStateChanged?.Invoke(CurrentState);
    }

    public void SetState(Dictionary<Type, BaseState> states)
    {
        _availableState = states;
    }
}