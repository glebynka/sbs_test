﻿using System;
using UnityEngine;

public class SheepsManager : MonoBehaviour
{
    [SerializeField] private LevelDataManager _levelDataManager;
    [SerializeField] private SheepSpawner _sheepSpawner;

    [SerializeField] private float _stopDistance = 0.2f;

    private Transform[] _waypoints;
    private Gate[] _gates;
    private float[] _distanceBetweenWaypoints;

    private void Awake()
    {
        Init();
    }

    private void Init()
    {
        _waypoints = _levelDataManager.CurrentLevel.GetWaypointsOfRoot();
        _distanceBetweenWaypoints = _levelDataManager.CurrentLevel.GetRootDistances();
        _gates = _levelDataManager.CurrentLevel.GetGates();
    }

    private void Update()
    {
        for (var i = 0; i < _sheepSpawner.Sheeps.Count; i++)
        {
            var nextIndex = i + 1;

            var distBetweenSheeps = (nextIndex < _sheepSpawner.Sheeps.Count)
                ? Vector2.Distance(_sheepSpawner.Sheeps[i].transform.position,
                    _sheepSpawner.Sheeps[nextIndex].transform.position)
                : _stopDistance;

            if (distBetweenSheeps >= _stopDistance)
            {
                _sheepSpawner.Sheeps[i].MoveSheep(_waypoints, _distanceBetweenWaypoints);
                //Debug.Log($"Sheep {_sheepSpawner.Sheeps[i].name} is walking");
            }
            else
            {
                _sheepSpawner.Sheeps[i].StopSheep();
                //Debug.Log($"Sheep {_sheepSpawner.Sheeps[i].name} is stopping");
            }

            // if (_gates.Length > 0)
            // {
            //     for (var j = 0; j < _gates.Length; j++)
            //     {
            //         if (!_gates[i].IsGateOpen)
            //         {
            //             _sheepSpawner.Sheeps[i].StopSheep();
            //         }
            //     }
            // }
        }
    }
}