﻿using UnityEngine;

public class SheepBehaviour : MonoBehaviour
{
    [SerializeField] private float _speed = 0.01f;

    private int _pointIndex;
    private float _moveСoefficient = 0;


    public void MoveSheep(Transform[] waypoints, float[] distBetweenPoints)
    {
        if (!gameObject.activeInHierarchy)
            return;

        if (_pointIndex < distBetweenPoints.Length)
        {
            _moveСoefficient += _speed / distBetweenPoints[_pointIndex];
            transform.position = waypoints[_pointIndex].position + (waypoints[_pointIndex + 1].position - waypoints[_pointIndex].position) * _moveСoefficient;

            if (_moveСoefficient >= 1)
            {
                _moveСoefficient = 0;
                _pointIndex++;

                TurnSheep(waypoints);
            }
        }
        else
        {
            EventsObserver.Publish(new SheepFinishedEvent(this));
        }
    }

    private void TurnSheep(Transform[] waypoints)
    {
        var nextIndex = _pointIndex + 1;

        if (nextIndex > waypoints.Length - 1)
            return;

        var dir = (waypoints[nextIndex].position - waypoints[_pointIndex].position).normalized;
        Debug.DrawRay(transform.position, dir, Color.cyan, 1);
    }

    public void StopSheep()
    {
    }

    public void SleepSheep()
    {
    }
}